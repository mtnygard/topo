# topo

Clojure library to apply a "topological" sort. That takes them from a
set of dependencies to a linear order.

## Latest version

No releases yet, feel free to use in `deps.edn` as:

```clojure
{:deps {topo {:git/url "https://gitlab.com/mtnygard/topo.git"
                      :git/sha "d914afe149311ab5f6014f977d3aa8bfbe56e16e"}
```

## Usage

Pass a map from "items" to sets of "items" they depend on. What's an
"item"? Any Clojure value.

```clojure
(require '[topo :as t])

(t/topo-sort {:a #{:b :c :d}, :b #{:c}})
;; => {:sorted (:d :c :b :a), :cycles []}

(t/topo-sort {:a #{:b :c :d}, :b #{:c}, :d #{:a}})
;; => {:sorted (:c :b), :cycles [#{:d :a}]}
```

The return value is a map with two keys. `:sorted` contains the
ordered list such that all dependencies occur before their
referent(s). `:cycles` contains a list of items that are involved in a
cycle. Right now there is no attempt to partition the cycles into
discrete clusters, so this list will have one set in it. In the
future, we may be more precise.

## Copyright

Copyright (C) 2020 Michael T. Nygard

## License

Distributed under the Eclipse Public License

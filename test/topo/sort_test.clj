(ns topo.sort-test
  (:require [topo.sort :as sut]
            [clojure.test :refer :all]))

(def good-sample
  {:des_system_lib   #{:std :synopsys :std_cell_lib :des_system_lib :dw02 :dw01 :ramlib :ieee}
   :dw01             #{:ieee :dw01 :dware :gtech}
   :dw02             #{:ieee :dw02 :dware}
   :dw03             #{:std :synopsys :dware :dw03 :dw02 :dw01 :ieee :gtech}
   :dw04             #{:dw04 :ieee :dw01 :dware :gtech}
   :dw05             #{:dw05 :ieee :dware}
   :dw06             #{:dw06 :ieee :dware :qcon}
   :dw07             #{:ieee :dware}
   :dware            #{:ieee :dware}
   :gtech            #{:ieee :gtech}
   :ramlib           #{:std :ieee}
   :std_cell_lib     #{:ieee :std_cell_lib}
   :synopsys         #{}})

(def good-sort
  '(:std :qcon :ieee :synopsys :ramlib :std_cell_lib :dware :gtech :dw01 :dw02 :dw06 :dw05 :dw07 :dw04 :dw03 :des_system_lib))

;; create a sample with a cycle
(def bad-sample
  (assoc good-sample :dw01 #{:dw04}))

(def expected-cycle
  #{:des_system_lib :dw03 :dw04 :dw01})

(deftest sort-without-cycles
  (is (= good-sort
        (:sorted
         (sut/topo-sort good-sample)))))

(deftest sort-returns-nil-with-cycles
  (is (= expected-cycle
        (first (:cycles (sut/topo-sort bad-sample))))))

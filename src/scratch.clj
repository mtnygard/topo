(ns topo
  (:require [clojure.set :refer :all]
            [clojure.walk :as walk]))

(defn dep
  "Constructs a single-key dependence, represented as a map from
   item to a set of items, ensuring that item is not in the set."
  [item items]
  {item (difference (set items) (list item))})

(defn empty-dep
  "Constructs a single-key dependence from item to an empty set."
  [item]
  (dep item '()))

(defn pair-dep
  "Invokes dep after destructuring item and items from the argument."
  [[item items]]
  (dep item items))

(defn default-deps
  "Constructs a default dependence map taking every item
   in the argument to an empty set"
  [items]
  (apply merge-with union (map empty-dep (flatten items))))

(defn declared-deps
  "Constructs a dependence map from a list containaining
   alternating items and list of their predecessor items."
  [items]
  (apply merge-with union (map pair-dep (partition 2 items))))

(defn deps
  "Constructs a full dependence map containing both explicitly
   represented dependences and default empty dependences for
   items without explicit predecessors."
  [items]
  (merge (default-deps items) (declared-deps items)))

(defn no-dep-items
  "Returns all keys from the argument which have no (i.e. empty) dependences."
  [deps]
  (filter #(empty? (deps %)) (keys deps)))

(defn remove-items
  "Returns a dependence map with the specified items removed from keys
   and from all dependence sets of remaining keys."
  [deps items]
  (let [items-to-remove (set items)
        remaining-keys  (difference (set (keys deps)) items-to-remove)
        remaining-deps  (fn [x] (dep x (difference (deps x) items-to-remove)))]
    (apply merge (map remaining-deps remaining-keys))))

(defn topo-sort-deps
  "Given a dependence map, returns either a list of items in which each item
   follows all of its predecessors, or a string showing the items among which
   there is a cyclic dependence preventing a linear order."
  [deps]
  (loop [remaining-deps deps
         result         '()]
    (if (empty? remaining-deps),
        (reverse result)
        (let [ready-items (no-dep-items remaining-deps)]
          (if (empty? ready-items)
              (str "ERROR: cycles remain among " (keys remaining-deps))
              (recur (remove-items remaining-deps ready-items)
                     (concat ready-items result)))))))

(defn topo-sort
  "Given a list of alternating items and predecessor lists, constructs a
   full dependence map and then applies topo-sort-deps to that map."
  [items]
  (topo-sort-deps (deps items)))

;;
;; implementation below uses a map as input
;;


(defn dep2
  "Constructs a single-key dependence, represented as a map from
   item to a set of items, ensuring that item is not in the set."
  [item items]
  {item (disj items item)})

(defn flatten-map
  "Return a set with every key and all their values (required to also be sets)"
  [m]
  (reduce (fn [s [k v]] (apply union s [#{k} v])) #{} m))

(defn map-kv [f m]
  (apply merge (map f (keys m) (vals m))))

(defn default-deps2
  "Constructs a default dependence map taking every item
   in the argument to an empty set"
  [items]
  (zipmap (flatten-map items) (repeat #{})))

(defn declared-deps2
  "Constructs a dependence map from a list containaining
   alternating items and list of their predecessor items."
  [items]
  (map-kv (fn [k v] (dep2 k v)) items))

(defn deps2
  "Constructs a full dependence map containing both explicitly
   represented dependences and default empty dependences for
   items without explicit predecessors."
  [items]
  (merge (default-deps2 items) (declared-deps2 items)))

(defn topo-sort2
  "Given a map of items to their predecessor sets, constructs a
   full dependence map and then applies topo-sort-deps to that map."
  [items]
  (topo-sort-deps (deps2 items)))

(topo-sort2 good-sample2)

(= (topo-sort good-sample) (topo-sort2 good-sample2))

(def good-sample2
  {:des_system_lib   #{:std :synopsys :std_cell_lib :des_system_lib :dw02 :dw01 :ramlib :ieee}
   :dw01             #{:ieee :dw01 :dware :gtech}
   :dw02             #{:ieee :dw02 :dware}
   :dw03             #{:std :synopsys :dware :dw03 :dw02 :dw01 :ieee :gtech}
   :dw04             #{:dw04 :ieee :dw01 :dware :gtech}
   :dw05             #{:dw05 :ieee :dware}
   :dw06             #{:dw06 :ieee :dware :qcon}
   :dw07             #{:ieee :dware}
   :dware            #{:ieee :dware}
   :gtech            #{:ieee :gtech}
   :ramlib           #{:std :ieee}
   :std_cell_lib     #{:ieee :std_cell_lib}
   :synopsys         #{}})

(def good-sample
  '(:des_system_lib   (:std :synopsys :std_cell_lib :des_system_lib :dw02 :dw01 :ramlib :ieee)
                      :dw01             (:ieee :dw01 :dware :gtech)
                      :dw02             (:ieee :dw02 :dware)
                      :dw03             (:std :synopsys :dware :dw03 :dw02 :dw01 :ieee :gtech)
                      :dw04             (:dw04 :ieee :dw01 :dware :gtech)
                      :dw05             (:dw05 :ieee :dware :qcon)
                      :dw06             (:dw06 :ieee :dware)
                      :dw07             (:ieee :dware)
                      :dware            (:ieee :dware)
                      :gtech            (:ieee :gtech)
                      :ramlib           (:std :ieee)
                      :std_cell_lib     (:ieee :std_cell_lib)
                      :synopsys         ()))

(def cyclic-dependence
  '(:dw01 (:dw04)))

(def bad-sample
  (concat cyclic-dependence good-sample))

(ns topo.sort
  "Topological sort of items.

  Based on Clojure example from Rosetta Code

  https://www.rosettacode.org/wiki/Topological_sort"
  (:require [clojure.set :as s]))

(defn dep
  "Constructs a single-key dependence, represented as a map from
   item to a set of items, ensuring that item is not in the set."
  [item items]
  {item (disj items item)})

(defn flatten-map
  "Return a set with every key and all their values (required to also be sets)"
  [m]
  (reduce (fn [s [k v]] (apply s/union s [#{k} v])) #{} m))

(defn map-kv [f m]
  (apply merge (map f (keys m) (vals m))))

(defn default-deps
  "Constructs a default dependence map taking every item
   in the argument to an empty set"
  [items]
  (zipmap (flatten-map items) (repeat #{})))

(defn declared-deps
  "Constructs a dependence map from a list containaining
   alternating items and list of their predecessor items."
  [items]
  (map-kv (fn [k v] (dep k v)) items))

(defn deps
  "Constructs a full dependence map containing both explicitly
   represented dependences and default empty dependences for
   items without explicit predecessors."
  [items]
  (merge (default-deps items) (declared-deps items)))

(defn no-dep-items
  "Returns all keys from the argument which have no (i.e. empty) dependences."
  [d]
  (filter #(empty? (get d %)) (keys d)))

(defn return
  [sorted-so-far cycles]
  {:sorted sorted-so-far
   :cycles cycles})

(defn remove-items
  "Returns a dependence map with the specified items removed from keys
   and from all dependence sets of remaining keys."
  [deps items]
  (let [items-to-remove (set items)
        remaining-keys  (s/difference (set (keys deps)) items-to-remove)
        remaining-deps  (fn [x] (dep x (s/difference (deps x) items-to-remove)))]
    (apply merge (map remaining-deps remaining-keys))))

(defn topo-sort-deps
  "Given a dependence map, returns either a list of items in which each item
   follows all of its predecessors, or a string showing the items among which
   there is a cyclic dependence preventing a linear order."
  [deps]
  (loop [remaining-deps deps
         result         '()]
    (if (empty? remaining-deps)
      (return (reverse result) [])
      (let [ready-items (no-dep-items remaining-deps)]
        (if (empty? ready-items)
          (return (reverse result) [(into #{} (keys remaining-deps))])
          (recur (remove-items remaining-deps ready-items)
            (concat ready-items result)))))))

(defn topo-sort
  "Given a map of items to their predecessor sets, constructs a
   full dependence map and then applies topo-sort-deps to that map."
  [items]
  (topo-sort-deps (deps items)))
